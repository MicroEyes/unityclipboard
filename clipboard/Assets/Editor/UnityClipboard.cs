﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Runtime.InteropServices;
using System;
using System.Text;

public class UnityClipboard
{
    [DllImport("User32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool IsClipboardFormatAvailable(uint format);

    [DllImport("User32.dll", SetLastError = true)]
    private static extern IntPtr GetClipboardData(uint uFormat);

    [DllImport("User32.dll", SetLastError = true)]
    private static extern IntPtr SetClipboardData(uint uFormat, IntPtr data);

    [DllImport("User32.dll", SetLastError = true)]
    private static extern bool EmptyClipboard();

    [DllImport("User32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool OpenClipboard(IntPtr hWndNewOwner);

    [DllImport("User32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool CloseClipboard();

    [DllImport("Kernel32.dll", SetLastError = true)]
    private static extern IntPtr GlobalLock(IntPtr hMem);

    [DllImport("Kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GlobalUnlock(IntPtr hMem);

    [DllImport("Kernel32.dll", SetLastError = true)]
    private static extern int GlobalSize(IntPtr hMem);

    private const uint CF_UNICODETEXT = 13U;

    [MenuItem("CONTEXT/Transform/Copy")]
    static void CopyData()
    {
        Debug.Log("Copy data started");

        string l_StrSrc = "This is test messagge." + DateTime.Now.Millisecond;
        //IntPtr l_nativePtrSrc = IntPtr.Zero;
        //IntPtr l_nativePtrSaved = IntPtr.Zero;
        try
        {
            Debug.Log("Text to copy '" + l_StrSrc + "'");
            if (!OpenClipboard(IntPtr.Zero))
                throw new Exception("Unable to open clipboard");

            if (!EmptyClipboard())
                throw new Exception("Unable to empty clipboard");


            byte[] l_bytes = Encoding.Unicode.GetBytes(l_StrSrc + "\0");
            int l_bytesToAllocate = l_bytes.Length * sizeof(byte);

            IntPtr l_ptrTarget = Marshal.AllocHGlobal(l_bytesToAllocate);
            if(l_ptrTarget == IntPtr.Zero)
                throw new Exception("Unable to allocate memory for source data.");
            
            l_ptrTarget = GlobalLock(l_ptrTarget);
            if (l_ptrTarget == IntPtr.Zero)
                throw new Exception("Unable to get global lock.");
            

            Debug.Log("5: Bytes Len: " + l_bytes.Length);
            Marshal.Copy(l_bytes, 0, l_ptrTarget, l_bytesToAllocate);            
            
            Debug.Log("6");
            if (!GlobalUnlock(l_ptrTarget))
            {
                throw new Exception("Unable to unlock global lock.");
            }

            Debug.Log("7");
            l_ptrTarget = SetClipboardData(CF_UNICODETEXT, l_ptrTarget);
            if (l_ptrTarget != IntPtr.Zero)
            {

                Debug.Log("Copy data finished succussfully");
            }
            else

            {
                Debug.LogError("Unable to set clipboard data");
            }

            Debug.Log("8");
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        finally
        {
            if (!CloseClipboard())
                Debug.LogError("Unable to close clipboard");
        }
        Debug.Log("Copy data end.");
    }

    [MenuItem("Clipboard/Close Clipboard")]
    static void CloseClipBoard_()
    {
        CloseClipboard();
    }

    [MenuItem("CONTEXT/Transform/Paste")]
    static void PasteData()
    {
        Debug.Log("Paste data started");

        try
        {
            if (!OpenClipboard(IntPtr.Zero))
            {
                throw new Exception("Unable to open clipboard");
            }

            IntPtr l_nativePtrSaved = GetClipboardData(CF_UNICODETEXT);
            if (l_nativePtrSaved != IntPtr.Zero)
            {
                string l_strData = string.Empty;
                var lpwcstr = GlobalLock(l_nativePtrSaved);
                if (lpwcstr != IntPtr.Zero)
                {
                    l_strData = Marshal.PtrToStringUni(l_nativePtrSaved);
                    if (!GlobalUnlock(lpwcstr))
                        throw new Exception("Unable to unlock global lock.");
                }
                else
                {
                    throw new Exception("Unable to get global lock.");
                }

                Debug.Log("Clipboard data '" + l_strData + "'");
            }
            else
            {
                throw new Exception("Unable to get clipboard data");
            }

        }
        catch(Exception e)
        {
            Debug.LogException(e);
        }
        finally
        {
            if (!CloseClipboard())
            {
                Debug.LogError("Unable to close clipboard");
            }
        }

        Debug.Log("Paste data end.");
    }

}